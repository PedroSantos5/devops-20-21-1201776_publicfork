# Class Assignment 4 Report

## 1.1 Analysis - Docker

Docker is open-source technology - and a container file format — for automating the deployment of applications 
as portable, self-sufficient containers that can run in the cloud or on-premises. Docker, Inc., although it 
shares a similar name, is one of the companies that cultivates the open-source Docker technology to run on 
Linux and Windows in collaboration with cloud providers like Microsoft (*[kubernetes-vs-docker][1]*).

While the idea of isolating environments is not new and there are other types of containerization software, 
Docker has grown to be the default container format in recent years. Docker features the Docker Engine, which 
is a runtime environment. It allows you to build and run containers on any development machine; then store or 
share container images through a container registry like Docker Hub or Azure Container Registry (*[kubernetes-vs-docker][1]*).

## 1.2 Implementation using Docker

For this implementation, I used my laptop running Linux Mint 20.1, which is based in Ubuntu 20.04.

I started by installing Docker as packages. First its dependencies **containerd.io 1.4.4-1** and 
**docker-ce-cli 20.10.6**, then the main Docker package **docker-ce 20.10.6**.

To verify that Docker is installed correctly:

```bash
$ sudo docker run hello-world
```

Then, to install Docker-Compose 1.29.1:

```bash
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
```

After that, I downloaded the **docker-compose** and **Dockerfiles** from the teacher's repository located at
https://bitbucket.org/atb/docker-compose-spring-tut-demo .

I just had to change the Dockerfile of the '**web**' container so that it downloads my remote repository using git
instead of the teacher's repository, and change the name of the .war file from 'basic' to 'demo':

```dockerfile
RUN git clone https://PedroSantos5@bitbucket.org/PedroSantos5/devops-20-21-1201776.git

WORKDIR /tmp/build/devops-20-21-1201776/ca2/Part2/demo

...

RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps
```

On my host machine, navigating to the directory where the docker-compose.yml file is present, I just had to build
the images and initialize them:

```bash
$ sudo docker-compose build
$ sudo docker-compose up
```

Both container's Dockerfiles will be read and executed when docker-compose starts. That is why each container's 
Dockerfile is placed a different folders with the name of the container, at the same level of the docker-compose file.

I tested that my containers were running as well as the app, by checking on a browser on my host machine:

http://localhost:8080/demo-0.0.1-SNAPSHOT/  to test the web app.

http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console   to open the h2 console.

To publish the images on Docker Hub, I started by creating an account.

To check the name and tag of the images, I ran:

```bash
$ sudo docker images
```

Now to login on my Docker Hub account through the command line:

```bash
$ sudo docker login
```

Before pushing the images, I had to tag them, so they are easier to identify:

```bash
$ sudo docker tag ca4_db mechapede5/pedro-santos-1201776:DB_container
$ sudo docker tag ca4_web mechapede5/pedro-santos-1201776:WEB_container
```

where **ca4_db** and **ca4_web** are the name of the images and **DB_container** and **WEB_container** are the tag
I set for the respective images.

Now to push these tagged images to my remote repository in Docker Hub 
(https://hub.docker.com/repository/docker/mechapede5/pedro-santos-1201776):

```bash
$ sudo docker push mechapede5/pedro-santos-1201776:DB_container
$ sudo docker push mechapede5/pedro-santos-1201776:WEB_container
```

To copy the database file from the container to a shared volume with my host machine, I started by
running a shell inside the container:

```bash
$ sudo docker exec -it ca4_db_1 bash
```

where **ca4_db_1** is the name of my running DB container.

Now inside the container through its shell, in directory '**/usr/src/app**' and copied the database file to the 
'**data**' folder which is shared with my host machine:

```bash
$ cp jpadb.mv.db ../data
```

After that was done, I exited the container's shell by typing `exit`, and stopped both containers by running:

```bash
$ sudo docker stop ca4_web_1
$ sudo docker stop ca4_db_1
```

**THIS IS THE END OF THE IMPLEMENTATION USING DOCKER.**

---

## 2.1 Analysis of an Alternative - Kubernetes

As applications grow to span multiple containers deployed across multiple servers, operating them 
becomes more complex. While Docker provides an open standard for packaging and distributing containerized 
apps, the potential complexities can add up fast. How do you coordinate and schedule many containers?
How do all the different containers in your app talk to each other? How do you scale many container instances? 
This is where Kubernetes can help (*[kubernetes-vs-docker][1]*).

The difference between Kubernetes and Docker is more easily understood when framed as a “both-and” question. 
The fact is, you don’t have to choose — Kubernetes and Docker are fundamentally different technologies that work 
well together for building, delivering, and scaling containerized apps (*[kubernetes-vs-docker][1]*).

Kubernetes is open-source orchestration software that provides an API to control how and where those containers 
will run. It allows you to run your Docker containers and workloads and helps you to tackle some of the operating 
complexities when moving to scale multiple containers, deployed across multiple servers (*[kubernetes-vs-docker][1]*).

Kubernetes lets you orchestrate a cluster of virtual machines and schedule containers to run on those virtual 
machines based on their available compute resources and the resource requirements of each container. Containers
are grouped into pods, the basic operational unit for Kubernetes. These containers and pods can be scaled to your
desired state and you’re able to manage their lifecycle to keep your apps up and running (*[kubernetes-vs-docker][1]*).

While it’s common to compare Kubernetes with Docker, a more apt comparison is Kubernetes vs. Docker Swarm. 
Docker Swarm is Docker’s orchestration technology that focuses on clustering for Docker containers—tightly 
integrated into the Docker ecosystem and using its own API (*[kubernetes-vs-docker][1]*).

A fundamental difference between Kubernetes and Docker is that Kubernetes is meant to run across a cluster 
while Docker runs on a single node. Kubernetes is more extensive than Docker Swarm and is meant to coordinate 
clusters of nodes at scale in production in an efficient manner. Kubernetes pods—scheduling units that can contain 
one or more containers in the Kubernetes ecosystem—are distributed among nodes to provide high availability (*[kubernetes-vs-docker][1]*).

While the promise of containers is to code once and run anywhere, Kubernetes provides the potential to 
orchestrate and manage all your container resources from a single control plane. It helps with networking, 
load-balancing, security, and scaling across all Kubernetes nodes which runs your containers. Kubernetes also 
has built-in isolation mechanism like namespaces which allows you to group container resources by access permission, 
staging environments and more. These constructs make it easier for IT to provide developers with self-service
resource access and developers to collaborate on even the most complex microservices architecture without 
mocking up the entire application in their development environment. Combining DevOps practices with containers 
and Kubernetes further enables a baseline of microservices architecture that promotes fast delivery and 
scalable orchestration of cloud-native applications (*[kubernetes-vs-docker][1]*).

Kubernetes and Docker work together. Docker provides an open standard for packaging and distributing 
containerized applications. Using Docker, you can build and run containers, and store and share container 
images. One can easily run a Docker build on a Kubernetes cluster, but Kubernetes itself is not a complete 
solution. To optimize Kubernetes in production, implement additional tools and services to manage security, 
governance, identity, and access along with continuous integration/continuous deployment (CI/CD) workflows
and other DevOps practices (*[kubernetes-vs-docker][1]*).


## 2.2 Implementation using Kubernetes

For this implementation, I used my laptop running Linux Mint 20.1, which is based in Ubuntu 20.04.

I started by installing Kompose, which allows the conversion of a docker-compose.yml file into files that Kubernetes
can read:

```bash
$ curl -L https://github.com/kubernetes/kompose/releases/download/v1.22.0/kompose-linux-amd64 -o kompose
$ chmod +x kompose
$ sudo mv ./kompose /usr/local/bin/kompose
```

Then, by installing Minikube. Minikube allows the creation of a cluster where Kubernetes can operate our services 
and deployments in pods:

```bash
$ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
$ sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

Finally, I installed Kubernetes:

```bash
$ curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

$ sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

On the directory where our docker-compose.yml is present [ca4/Kubernetes](), I initialized a terminal, where I 
started by initiating minikube:

```bash
$ minikube start
```

This will create a VM where the pods will be created. It uses one hypervisor from the ones available in the host machine.
In my case, it created a VM using the kvm2 driver since I installed KVM in the previous assignment.

We now have to convert the docker-compose.yml file into files readable by Kubernetes using a Kompose command:

```bash
$ kompose convert --volumes hostPath
```

The additional option on the command will solve a warning related with a persistentVolumeClaim file that would
be created in the absence of that additional option. Doing this ultimately impossibilites the correct use of the data volume
that we used in the Docker implementation. I did not try to solve this issue as it was secondary, and I was running
short on time, but I found useful information regarding that here 
https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/ .
So, additional files were created after running this command. I had the need to edit some of them to solve 
additional issues, namely, the use of NodePort (an abstract way to expose an application running on a set of Pods 
as a network service), and the images to be used for the Pods, that needed to be pulled from my Docker Hub repository. 
The files are present in [ca4/Kubernetes]().

Before applying the files using Kubernetes, we need to log into Docker Hub so Kubernetes can automatically pull the
images from there:

```bash
$ sudo docker login
```

I could now apply these files using Kubernetes, which will initialize the creation of pods following those files'
specifications:

```bash
$ kubectl apply -f .
```

These will apply each of the files present in the current folder. As such, an error is expected to be thrown because
Kubernetes will also try to apply the docker-compose.yml file, but we can ignore this error since the other files
are correctly applied. With this, the creation of the pods containing our containers will start.

We can follow the status of the creation by using:

```bash
$ kubectl get pods
```

Or, for a more detailed view:

```bash
$ kubectl describe pods
```

We can also look at the services of each deployment:

```bash
$ kubectl get svc
```

After the "**web**" and "**db**" pods are **running**, we must use a special command to see what is the ip of each pod.
Since Kubernetes starts the pods with random ips (not the ones detailed in the docker-compose.yml) and because the 
Minikube cluster also does the same, we will need to use the actual ip of the "**db**" pod and apply it to the
application.properties file of our app present in the "**web**" pod:

```bash
$ kubectl get pod -o wide
```

We should take note of that ip somewhere, as we will need it.
We must now "enter" inside the "**web**" pod, by running a shell inside it:

```bash
$ kubectl exec --stdin --tty [pod-name] -- /bin/bash
```

In [pod-name] we should use the actual name of the "**web**" pod, which should contain some random numbers and letters.
After the shell is running and we are inside, we must first install a text editor so we can edit the required file.
I opted for installing nano:

```bash
$ apt install nano
```

Then, by opening the correct file to be edited:

```bash
$ nano src/main/resources/application.properties
```

I changed the ip for the connection of the db, which is running in the "**db**" pod. In my case, the ip of said pod
was 172.17.0.3 :

```
server.servlet.context-path=/demo-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://172.17.0.3:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

After that, I rebuilt the app, so the change is applied:

```bash
$ ./gradlew clean build
```

Then, I had to copy over the SNAPSHOT.war file generated to the tomcat webapps folder, by doing:

```bash
$ cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
```

Now I can leave this shell by doing:

```bash
$ exit
```

We are now ready to open the service and see through a browser on the host machine both the app and db running:

```bash
$ minikube service web
```

This command will automatically open our browser with the ip of the cluster pointing to the port of the "**web**"
pod.

We should navigate to the created SNAPSHOT by adding "/demo-0.0.1-SNAPSHOT" to the url. The image below is the result
I achieved:

![app](Kubernetes/images/app.png)

Then, to see the h2 console, we just need to add "h2-console" to the url. The image below is the result I achieved:

![h2console](Kubernetes/images/h2-console.png)

Note that I changed the JDBC URL to match the ip of the "**db**" pod. As you can see I tested the connection, and later
connected to the database to confirm that there was a table of EMPLOYEES.

**THIS IS THE END OF THE IMPLEMENTATION USING KUBERNETES**

## 3. References (not rendered)

[1]: https://azure.microsoft.com/en-us/topic/kubernetes-vs-docker/#:~:text=A%20fundamental%20difference%20between%20Kubernetes,production%20in%20an%20efficient%20manner.