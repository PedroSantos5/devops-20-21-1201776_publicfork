# Class Assignment 5 Report

## Part 1 Implementation - CI/CD pipelines with Jenkins

For this implementation, I used my laptop running Linux Mint 20.1, which is based on Ubuntu 20.04.

I started by downloading Jenkins and use it directly through its *.war* file. I chose the LTS version.

To run Jenkins, we just need to run the following command on a terminal, where the file is present:

```bash
$ java -jar jenkins.war
```

With Jenkins up and running, we should go to a browser and navigate to:

```
http://localhost:8080/
```

First we will need to configure the user and its credentials. It is very straightforward as there are 
indications on the screen. When prompted, we should choose to install the default plugins. We will need 
to install some later though.

To make use of the Jenkins credentials feature, ***I turned my remote repository private.*** This will 
inevitably cause some problems when executing the past class assignments so please beware of that.

On the Jenkins credentials setup page, I defined my Bitbucket username(email) and its password, along 
with the ID. This ID will be used on our **Jenkinsfile**. This way we can authenticate with our credentials 
without displaying them directly on the Jenkinsfile.

Next, I created a Jenkinsfile. This is the file where we define our pipeline. I placed this file in 
[ca2/Part1](../ca2/Part1). 
The pipeline defined was changed many times, until all stages completed successfully.

On Jenkins, we should create a new Job and choose the option 'Pipeline'. On this pipeline configuration, 
we should define *script from SCM* and set the **Script Path** to [ca2/Part1/Jenkinsfile](../ca2/Part1/Jenkinsfile) 
where the Jenkinsfile 
is located. This means that our pipeline will be the one described in the Jenkinsfile.

For demonstration purposes, the Jenkinsfile used is placed at [ca5/Part1/Jenkinsfile](Part1/Jenkinsfile).

Below is a screenshot of my successfull attempt running the Jenkins Job for the app 
located at [ca2/Part1](../ca2/Part1).

![JenkinsPart1](Part1/Images/JenkinsPart1.png)

***THIS IS THE END OF THE IMPLEMENTATION USING JENKINS FOR PART 1.***


---

## Part 2 Implementation - CI/CD pipelines with Jenkins

For this implementation, I used my laptop running Linux Mint 20.1, which is based on Ubuntu 20.04.

In Part 2, the app to be used in the Jenkinsfile pipeline is the one located at [ca2/Part2/demo](../ca2/Part2/demo). 

As we will be using our Docker Hub repository, on Jenkins I created an ID for the credentials to be used 
to authenticate on Docker Hub.

This new pipeline requires a new stage **Publish Image** that will build a Docker image and publish it to our Docker 
Hub repo. This also requires a **Dockerfile**. The Dockerfile I used is the one developed in ca4, located at 
[ca4/Docker/web/Dockerfile](../ca4/Docker/web/Dockerfile) . Nevertheless, I placed a copy of this Dockerfile 
at [ca2/Part2/demo](../ca2/Part2/demo), as well as the new 
Jenkinsfile to be used on a new Job Pipeline in Jenkins.

The mentioned Dockerfile clones my Bitbucket repository that at the time during the ca4 implementation was public. 
Since my repo is now private, I created a public Fork of my repo, which will act as a copy of my private repo but 
is public. This new public fork will be the one that will be cloned during the container creation. I did this to 
not use any credentials displayed on my Dockerfile.

The main effort of this implementation was the use of the correct syntax in the Jenkinsfile, as there were more 
stages to define. The syntax also changed depending on what OS the host machine running Jenkins was using.

Here I will explain what I did for some of the problems I encountered:

- There was a problem during the **Publish Image** stage so I installed some Docker plugins on Jenkins and that 
  particular error was solved (Plugins: Docker, Docker Pipeline, Docker API, Docker-build-step).

- There was a problem using docker commands with authorization on my linux machine so I ran a couple of commands 
  but ultimately only the last one solved the problem:

```bash
$ sudo groupadd docker
$ sudo usermod -aG docker $USER
$ newgrp docker   #to update the configuration
$ docker run hello-world   #to see if this command runs without sudo.
$ sudo systemctl restart docker    #to restart the docker daemon.
$ sudo chmod 666 /var/run/docker.sock
```

- The rest of the problems were focused on the syntax of the Jenkinsfile but after much troubleshoot, 
  my Jenkins build succeeded. Below is a screenshot of my successful attempt running the Jenkins Job for the 
  app located at [ca2/Part2/demo](../ca2/Part2/demo).

![JenkinsPart2](Part2/Jenkins/Images/JenkinsPart2.png)

***THIS IS THE END OF THE IMPLEMENTATION USING JENKINS FOR PART 2.***

---

## Part 2 - Analysis of an Alternative - CI/CD pipelines with Buddy

Buddy, as well as Jenkins, is an automation server that allows building, deploying and automating any project.

The main differences between the two comprise (*[buddy.works][1]*):

- **Pipeline setup:** Where in Buddy it takes a 15-minute configuration via GUI with an optional instant export 
to YAML, in Jenkins the setup can take 6-10 hours as every action
  requires a dedicated plugin that needs to be troubleshot for functionality.
  
- **Performance:** In Buddy, dependencies and Docker layers are cached
in isolated containers optimized for performance, while in Jenkins, to improve performance, 
you need to manually tweak every plugin or upgrade the machine.

- **Maintenance:** Buddy has an intuitive UI/UX allows even non-tech users
to create and manage pipelines. Jenkins requires a full-time DevOps engineer
to configure, monitor and manage your pipelines.
  
---
  

## Part 2 - Implementation of an Alternative - CI/CD pipelines with Buddy

This implementation is not dependent on any host machine, which is a plus from the start.

I started by registering at https://buddy.works/ through my Bitbucket account. This immediately sets my remote repo
to be used for any pipeline. This integration eases many processes from the initial pipeline setup to 
post-job commits. I also integrated my Docker Hub repository, as you can see below:

![BuddyIntegrations](Part2/Buddy/Images/BuddyIntegrations.png)

After creating a pipeline, every step is defined in Actions, and they normally run in separate docker containers 
of our choosing (nothing depends on our machine!). Below is a screenshot of the Actions I defined for my pipeline.

![BuddyInitialActions](Part2/Buddy/Images/BuddyInitialActions.png)

The first action will run the gradle tasks on our app located at [ca2/Part2/demo](../ca2/Part2/demo) in a 
container with Gradle 6.8.3 (to match the gradlew version of our app).
Below is a view of the Environment setup for this container.

![GradleStagesEnvironment](Part2/Buddy/Images/GradleStagesEnvironment.png)

For the actual code, below is what the container will do:

![GradleStagesCode](Part2/Buddy/Images/GradleStagesCode.png)

Since I did not find a way to report the junit test, the javadoc and the archives as we did in Jenkins, I decided
to copy those files to folders located in [ca5/Part2/Buddy/](Part2/Buddy). After the gradle tasks are complete and 
the report files were copied to another location, I ran `gradle clean` to clean everything the gradle tasks created
as they are no longer needed. This will also ensure that the push being performed in the next action only uploads the 
report files to my repository.

Now to push the created reports to my repository, I created an Action that does just that. Below is a screenshot 
of this Action's setup:

![PushReportOptions](Part2/Buddy/Images/PushReportOptions.png)

For the final step, I created an Action that will create a Docker Image from the Dockerfile located at
[ca2/Part2/demo](../ca2/Part2/demo) and push it to my Docker Hub repository. Below is a screenshot of the
setup for this Action:

![DockerImageSetup](Part2/Buddy/Images/DockerImageSetup.png)

As for the Options for this Action:

![DockerImageOptions](Part2/Buddy/Images/DockerImageOptions.png)

After setting up all these Actions, my build succeeded. Here is a screenshot of the pipeline report:

![BuddyPipelineReport](Part2/Buddy/Images/BuddyPipelineReport.png)

Below we can see that Buddy pushed a new commit with the generated reports:

![BitbucketBuddyCommit](Part2/Buddy/Images/BitbucketBuddyCommit.png)

![BitbucketBuddyCommitInfo](Part2/Buddy/Images/BitbucketBuddyCommitInfo.png)

We can also see that the Docker Image created was pushed to my Docker Hub repository:

![DockerhubRepo](Part2/Buddy/Images/DockerhubRepo.png)

***THIS IS THE END OF THE IMPLEMENTATION USING BUDDY FOR PART 2.***

---

# Personal Note

I found Buddy much easier to work with than Jenkins, starting by the performance point-of-view. Working 
on my old laptop is very frustrating, so having the chance to make cpu/ram heavy operations in a remote machine 
(in this case a container provided by Buddy) made everything much faster. On a second note, the fact that we can easily define our
pipeline in the Buddy web GUI is a huge bonus. On Jenkins, for every edit I made to the Jenkinsfile, I had to commit
that change everytime so that Jenkins can go pick up the updated Jenkinsfile. To put it into perspective, I needed
two full days to implement the assigment for Part 1 and Part 2 using Jenkins, whereas for the implementation of Part 2
with Buddy I just needed one morning.

---

# References (not rendered)

[1]: https://buddy.works/vs/jenkins?utm_source=adwords&utm_medium=cpc&utm_campaign=txt_jenkins_eu&utm_term=jenkins&utm_campaign=%5BTXT%5D+-+Jenkins+-+EU&utm_source=adwords&utm_medium=ppc&hsa_acc=3810215883&hsa_cam=10435708214&hsa_grp=107199003521&hsa_ad=445451397564&hsa_src=g&hsa_tgt=kwd-14082300&hsa_kw=jenkins&hsa_mt=b&hsa_net=adwords&hsa_ver=3&gclid=CjwKCAjwn6GGBhADEiwAruUcKjWfbruRPXkMDc7SpnNfzjkIO8MtvlCmMLCHOV_uNw6KZ6Yu2I_vEBoCmW4QAvD_BwE