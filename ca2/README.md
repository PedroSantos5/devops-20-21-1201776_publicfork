# Class Assignment 2 Report

## Part 1 - Gradle tasks

I started by downloading the Gradle Basic Demo from (https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/).
I added it to folder [ca2/Part 1](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/ca2/Part%201/).

For each step of the assignment, a different issue was created in my personal
[repository](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/).

The commands shown were run on Git Bash, inside the folder [ca2/Part 1](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/ca2/Part%201/).

----

After testing the commands to build the basic demo, run the server and run the client, present in the [readme](Part1/README.md) file
of the Gradle Basic Demo, I made the first implementation of the new feature asked - **Add a new task to execute to server.**
To do this, I edited the [build.gradle](Part1/build.gradle) file and added the following code to add the new task:

```groovy
task runServer(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat server on localhost:59001"

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'
}
```
After that, to run the new task created, I ran the following command:
```bash
$ ./gradlew runServer
```
The server is now up and running until the process is stopped.

-------------------------------
For the next step, it was asked to develop a **simple unit test and update the gradle script** so that it is able to run
the test.
The code for the test was provided and is as follows:

```java
package basic_demo;

import org.junit.Test;

import static org.junit.Assert.*;

public class AppTest {
    @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
}
```

After creating the new class of the test and confirming that the test has passed,
the following dependency was added to the gradle script:

```groovy
testImplementation 'junit:junit:4.12'
```
I later noticed that there was no difference if I instead used the line:
```groovy
implementation 'junit:junit:4.12'
```
Now, to make sure that the test is executed when the script runs, I used the following command:

```bash
$ ./gradlew build
```
Or alternatively:

```bash
$ ./gradlew test
```

------

Next, it was asked that we create a new **task of type "Copy"** to be used to make a backup of the sources of the
application.

For that, I added the following code to the [build.gradle](Part1/build.gradle) file:

```groovy
task makeCopy(type: Copy) {
    group = "DevOps"
    description = "Creates a copy of the 'src' folder in a 'backup' folder"

    from 'src'
    into 'backup'
}
```
Now to run the task:
```bash
$ ./gradlew makeCopy
```

This created a folder called **"backup"** in [ca2/Part 1](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/ca2/Part%201/)
that contais a copy of the **"src"** folder.

-----

Finally, we should create a new **task of type "Zip"** to be used to make an archive of the sources of the application.
To achieve that, I added the following code to the [build.gradle](Part1/build.gradle) file:


```groovy
task makeZip(type: Zip) {
    group = "DevOps"
    description = "Creates a zip archive of the 'src' folder"

    archiveFileName = 'src.zip'
    destinationDirectory = file('zipFolder')
    from 'src'
}
```

To run the new task, I used:
```bash
$ ./gradlew makeZip
```

This creates a folder in [ca2/Part 1](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/ca2/Part%201/)
called **"zipFolder"**, where inside is present the file **"src.zip"**, which is an archive of the
**"src"** folder.

**After everything is done (markdown included), the repository should be tagged "ca2-part1".**

---

## Part 2 - Analysis, Design and Implementation using Gradle

### 1. Analysis - Gradle as Build Tool

**Gradle** is an open-source build tool with a focus on build automation and support for multi-language development.
If we are building, testing, publishing, and deploying software on any platform, Gradle offers a flexible
model that can support the entire development lifecycle from compiling and packaging code to publishing websites.
Gradle has been designed to support build automation across multiple languages and platforms including Java, Scala,
Android, Kotlin, C/C++, and Groovy, and is closely integrated with development tools and continuous integration
servers including Eclipse, IntelliJ, and Jenkins (*[Gradle][3]*). **Gradle** build scripts are written using a **Groovy**
or **Kotlin** DSL, and completes tasks quickly by reusing outputs from previous executions, processing only inputs
that changed and executing tasks in parallel (*[GradleTutorial][4]*). It is one of the most popular build tools for
java projects, as it derives all the best and integrates will with **Ivy**, **Ant** and **Maven**.

### 2. Design

For this assignment, the goal is to convert the basic version of the application used in [ca1](../ca1) to Gradle
(instead of Maven).

We should create e new branch called "**tut-basic-gradle**" where we will develop the features for this assignment.

We will then start a new gradle spring project with dependencies (Rest Repositories; Thymeleaf; JPA; H2) in
(https://start.spring.io/).

After extracting the generated zip file, we will have an "empty" spring application that can be built using gradle.

After deleting the 'src' folder of this new project, we should copy the 'src' folder from the basic version of the app
used in [ca1](../ca1), along with the files 'webpack.config.js' and 'package.json' into our new project.
Inside the 'src' folder that we will be using, we should delete the folder src/main/resources/static/built/ since it
should be generated anew from the javascript by the tool webpack.

To experiment the application initialization we should use `./gradlew bootRun`. It is expected that the web page
http://localhost:8080 will be empty, since gradle is still missing the plugin to handle the frontend code.

So, as a next step, we should add the gradle plugin **org.siouan.frontend** to the project, so gradle is able the manage
the frontend. The code to be added is detailed in the [Implementation using Gradle](#3-implementation-using-gradle) section.

After dealing with the dependencies, we can use `./gradlew build` to build the application and apply the changes and
then run again `./gradlew bootRun` to execute the application and confirm that the frontend is now working.

Now, we should create a custom task that will copy the generated .jar file to a folder named 'dist' located at the
project root folder level, and then another task to delete all the files generated by webpack in
'src/resources/main/static/built/'. This new task should be executed automatically by gradle before the task **clean**.

After the development of the new features is complete, we should merge the changes made in the branch created with the
master branch.

After writing this class assignment report, the repository should be tagged "**ca2-part2**".

### 3. Implementation using Gradle

Every command was ran using **Git Bash**.
I started by creating a new branch to develop the Part 2 of this class assignment.

We can start by checking the existing branches by doing:

```bash
$ git branch
```

I named the new branch "**tut-basic-gradle**":
```bash
$ git branch tut-basic-gradle
```
And move the HEAD pointer to this new branch:
```bash
$ git checkout tut-basic-gradle
```
Alternatively, I could have used the following command to create the new branch and immediately checkout to it:
```bash
$ git checkout -b tut-basic-gradle
```

Now we're ready to start making changes.

I started by creating a new folder named "Part2" inside the folder "ca2" ([ca2/Part2](../ca2/Part2)).

Next, by going to (https://start.spring.io), we can generate a new Gradle project with some dependencies already coupled
to it:

![Spring Initializr](Part2/demo/images/spring_initializr.png)

After choosing the options above, we just need to click on GENERATE to download the new project.

The project root folder should be named "**demo**". I copied this folder and pasted it into the "Part2" folder
created earlier. Now, open a Git Bash window inside this folder "**demo**".

Run the following command to check the available Gradle tasks:

```bash
$ ./gradlew tasks
```

We will see a list of default Gradle tasks. To check the tasks in more details, run the following command:
```bash
$ ./gradlew tasks --all
```
And to see more details about a specific task, do:
```bash
$ ./gradlew help --task <task>
```

Next, we should delete the "**src**" folder inside the "**demo**" directory, as we want to use the code from
the basic tutorial.

At this point, I opted for making a commit. I staged the changes, committed and pushed the new branch and commit to my
remote repository:

```bash
$ git add .
$ git commit -m "Demo folder already without 'src' folder."
$ git push -u origin tut-basic-gradle
```

After that, I copied the "**src**" folder from ([ca1/tut-basic](../ca1/tut-basic)) present in my repository and pasted
it into the [ca2/Part2/demo](Part2/demo) directory.
We also need to copy the "**package.json**" and "**webpack.config.js**" from the same [ca1/tut-basic](../ca1/tut-basic) directory
and paste it along with the "**src**" folder that we just copy-pasted.

Before running the application, we need to delete the "**demo/src/main/resources/static/built**" folder, since
this folder should be generated from the javascript by the tool webpack.

We are now ready to run the application. Just type the following in Git Bash:
```bash
$ ./gradlew bootRun
```

At this point, I encountered an error when the system was running the **compileJava** task:

![bootRun error](Part2/demo/images/bootRun_error.png)

I realised this error must have originated from the changes I made in the ***Class Assignment 1***, so I decided to try again
with the original **tut-basic** code. For that, I deleted everything inside [ca2/Part2/demo](Part2/demo). Then, copied
the "**src**" folder present in the original template repository that the teacher
provided (https://bitbucket.org/atb/devops-21-rep-template/src/master/ca1/tut-basic/) into my [ca2/Part2/demo](Part2/demo)
directory. I also copied the "**package.json**" and "**webpack.config.js**" files that were already mentioned. Since
this project was clean, there was no "src/main/resources/static/built" folder to delete. I was now ready to try running
the application again by doing:
```bash
$ ./gradlew bootRun
```

The app was now running, but the page located at http://localhost:8080 was empty:

![empty 8080 frontend](Part2/demo/images/empty_8080_frontend.png)

This is because Gradle is missing the plugin to deal with the frontend code.

To solve this, we will add the Gradle plugin "**org.siouan.frontend**" to the project, so that Gradle is able to
properly manage the frontend. We should add the following line to the **build.gradle** file:

```groovy
id 'org.siouan.frontend' version '1.4.1'
```

![siouan_plugin](Part2/demo/images/siouan_plugin.png)

We also need to configure the previous plugin by adding:

```groovy
frontend {
    nodeVersion = '12.13.1'
    assembleScript = 'run webpack'
}
```

![siouan_config](Part2/demo/images/siouan_config.png)

Now, updating the scripts section/object in the **package.json** file, to configure the execution of webpack:

```json
  "scripts": {
    "watch": "webpack --watch -d",
    "webpack": "webpack"
  },
```
![package_json_scripts](Part2/demo/images/package_json_scripts.png)

At this point, I made another commit to submit the changes to my remote repository:
```bash
$ git add .
$ git commit -m "Added plugins and configuration to build.gradle and package.json"
$ git push
```

If everything is set, we will build the application to apply the changes made:

```bash
$ ./gradlew build
```

The tasks related to the frontend are executed and the frontend code is generated.
Now we can try running the application to see if it worked:
```bash
$ ./gradlew bootRun
```

By checking the http://localhost:8080 again, we can see that the content now appears as it should:

![working 8080 frontend](Part2/demo/images/working_8080_frontend.png)

With the build of the application, the "node" and "node_modules" folders were created inside the "**demo**" directory.
As these folders are not supposed to be saved in the repository, I added the following lines to the **.gitignore** file
of the repository:

```
ca2/Part2/demo/node
ca2/Part2/demo/node_modules
```

Now, before developing a new Gradle task, I created an issue in Bitbucket associated with it.

The new task should copy the generated .jar file to a folder named "**dist**" located at the project root folder level.
Thus, I added the following code to the **build.gradle** file:
```groovy
task makeJarCopy(type: Copy) {
	group = "DevOps"
	description = "Creates a copy of the generated .jar file in a 'dist' folder located at the project root folder level."

	from 'build/libs/demo-0.0.1-SNAPSHOT.jar'
	into 'dist'
}
```

To run this new task, we should do:
```bash
$ ./gradlew makeJarCopy
```

We should confirm the task did what was supposed to, by checking that a folder "**dist**" was created inside the
"**demo**" directory.

With the task working as intended, I did a new commit:
```bash
$ git add .
$ git commit -m "Added gradle task to copy jar file. Resolves #7."
$ git push
```

With this, the issue created in Bitbucket is automatically closed.

Now, we should create a new issue related with the development of the next task.

The new Gradle task should delete all files generated by webpack (located at "src/resources/main/static/built/).
This new task should be automatically executed by Gradle before the execution of the task "**clean**".

Thus, I developed and added the following code to the "**build.gradle**" file:
```groovy
task deleteWebpackFiles(type: Delete) {
	group = "DevOps"
	description = "Deletes all files in the 'src/resources/main/static/built/' folder."

	delete fileTree('src/main/resources/static/built/')
}

clean {
    dependsOn deleteWebpackFiles
}
```

Alternatively, we could add the following line instead of the "clean" scope from above:
```groovy
clean.dependsOn deleteWebpackFiles
```

To test this new task, I ran the default "**clean**" task:
```bash
$ ./gradlew clean
```

The "**clean**" task by itself should only delete the build directory. I checked and noticed that the files inside
'**src/main/resources/static/built/**' were gone, so this proves that before Gradle ran the "**clean**" task, the
"**deleteWebpackFiles**" task run first. The created task also runs standalone, in case we only want to delete the
aforementioned files.

As the Gradle tasks to be developed for this assignment were working properly, I committed the changes:
```bash
$ git add .
$ git commit -m "Added 'deleteWebpackFiles' gradle task. Resolves #8."
$ git push
```

This will resolve the issue created in Bitbucket, but for now, we should merge the created branch with "**master**":
```bash
$ git checkout master
$ git merge tut-basic-gradle
$ git push
```

The implementation using Gradle is now complete.

**At the end of Part 2 of this assignment, the repository should be marked with the tag "**ca2-part2**".**



### 4. Analysis of an Alternative - Scala Sbt as Buid Tool

**Scala Sbt** uses the Scala language in all build directive files. As a result, we have the power (and libraries) of
an extensible build system backed by a mature multi-paradigm programming language (in common, for instance,
with Scons which uses python for build directive files) (*[sbt-cpp][1]*).

Scala is statically typed, which means that **sbt** compiles our build directive files before running a build.
This allows the tool to catch considerably more errors than is possible with a build tool based around a
dynamically typed language (e.g. Python). This also means that a large class of problems with infrequently
used targets are caught without the target having to be built/run.
Scala runs on the Java virtual machine, deferring many platform-specific problems in build
construction (filesystem paths, running external processes etc) to the Java runtime (*[sbt-cpp][1]*).

Therefore, **sbt** is built for Scala and Java projects, and features an incremental compilation and
interactive shell. The `build.sbt` file is a Scala-based DSL to express
parallel processing task graph. Typos in `build.sbt` will be caught as a compilation error  (*[Scala-sbt][2]*). This file is very similar
and serves the same purpose of the `build.gradle` file present in projects built with **Gradle**.

Thus, after searching for various alternatives to Gradle (namely Maven, Bazel, Ant, Grunt, MSBuild, etc), I opted for trying
out the **Scala Sbt**, since it would work flawlessly with java projects and because I found good documentation.
That would later
prove to not be enough to implement what was asked in this assignment. The difficulty of understanding the syntax of a
new programming language and the lack of building examples on the internet was just to great.

### 5. Implementation of an Alternative - Scala Sbt as Buid Tool

Unfortunately, I was unable to build the project provided by the teacher using **Scala Sbt**. I also could not find a
way to create the simple tasks I created using **Gradle**. I will just explain what I did try to make it work.

After installing **Scala Sbt** I tried to get a better understanding of how it works by following the initial
tutorial present in the [Sbt-by-Example](https://www.scala-sbt.org/1.x/docs/sbt-by-example.html) page of the Scala Sbt
documentation.

I then tried to copy over the 'src' folder from the basic version of the application used in [ca1](../ca1) and the
other two files, just like I did with the Gradle implementation. Then the real problem appeared: How to add
the required dependencies and plugins to make Sbt build the app successfully?

I understood that the code to be included in the `build.sbt` file to add dependencies should be something like:

```sbt
libraryDependencies += groupID % artifactID % revision
```

I followed that pattern for the various dependencies used in the Gradle implementation with no luck.
I also searched on the web for examples and for projects using such dependencies but there were not that many that could
help me to begin with. There was always something missing. At some point I also became aware that there is an
additional file in 'project/' called '**plugins.sbt**' where additional code was needed to activate the use of certain
plugins. Again, I could not make the build have success even with certain plugins added.
After trying many different things, I gave up and focused on trying to at least create the custom tasks needed for this
assignment.

I started by looking up the documentation for ways to create the custom tasks. As with Gradle, they should be
created in the `build.sbt` file, with the syntax being something like:

```sbt
lazy val intTask = taskKey[Int]("An int task")
lazy val stringTask = taskKey[String]("A string task")
```

Where the name of the `val` is used when referring to the task in Scala code and at the command line.
The string passed to the `taskKey` method is a description of the task.
The type parameter passed to `taskKey` (here, Int) is the type of value produced by the task.

Then, to define the task:

```sbt
intTask := 1 + 2

stringTask := System.getProperty("user.name")

sampleTask := {
   val sum = 1 + 2
   println("sum: " + sum)
   sum
}
```

A task is evaluated on demand. Each time `sampleTask` is invoked, for example, it will print the sum.
If the username changes between runs, `stringTask` will take different values in those separate runs.
(Within a run, each task is evaluated at most once.)
In contrast, settings are evaluated once on project load and are fixed until the next reload
(*[Sbt Tasks](https://www.scala-sbt.org/1.x/docs/Tasks.html)*).

The problem is, for our custom tasks, the use of paths is necessary. Since I do not know any Scala syntax, I tried
to search for ways to implement it.
After looking up some websites, I tried using some customized plugins and libraries to make use of a simpler syntax,
as mentioned in
https://mungingdata.com/scala/filesystem-paths-move-copy-list-delete-folders/. This website then
redirects to the git repository of the developed feature (https://github.com/com-lihaoyi/os-lib).
I imported the plugin and dependencies needed, to try
and make use of the command:

```scala
val wd = os.pwd
os.copy.into(wd / "File.txt", wd / "folder1")
```

This would copy the file `File.txt` into the folder `folder1`, both at the project root level.
The problem is, I cannot use this syntax in the `build.sbt` file. It can only be used as code in some Scala class.
I then tried to register tasks using code written in Scala tasks.

I tried to use the [Play sbt tasks](https://github.com/jaroop/play-sbt-tasks) plugin, but there was some problem with the
imports. I then tried to manually write some of the classes needed and add them to this project. It still did not work.

To summarize, I could not even develop the custom tasks needed using **Scala sbt**. I do not think it is that difficult
to use this build tool, but my lack of knowledge of the Scala language was a problem. I would need some more time
to learn and try to correctly implement this alternative for this assignment.



### 6. References (not rendered)

[1]: https://github.com/d40cht/sbt-cpp

[2]: https://www.scala-sbt.org/

[3]: https://github.com/gradle/gradle

[4]: https://www.vogella.com/tutorials/GradleTutorial/article.html