# Class Assignment 3 Report

## Part 1 - Virtualization with Vagrant

For this first part of the assignment, I already set up the Virtual Machine using VirtualBox following the steps provided by the teacher
in **devops05.pdf**.

The objective of this first part is to build and execute the previous applications we developed in the two previous 
assignments.

I started by connecting with my VM through ssh from my host machine:
```
ssh pedro@192.168.56.5
```

Now inside the VM, I cloned my personal repository into a new folder named "**MyRepo**".

Inside the folder "***ca1/tut-basic**" I ran the command:

```bash
$ ./mvnw spring-boot:run
```

This will build and run the application used in Class Assignment 1. There was no need to install maven since
I am using the bundled maven wrapper.

To make sure the application is now running flawlessly, I needed to go to the page located at **http://192.168.56.5:8080/**
on my host machine. Everything was working fine as the frontend was displaying the initial table.

Then I tested the application developed in "**ca2/Part2/demo**". After navigating to that directory, I needed to give
the **execute** permission to the "gradlew" file by using:

```bash
$ chmod +x  gradlew
```

This alone would not be enough, so I ran the build command with the **sudo** command:

```bash
$ sudo ./gradlew build
```

Again, there was no need to install Gradle on this VM since I was using the bundled gradle wrapper. 
The build was successful, so I proceeded to boot the application:

```bash
$ sudo ./gradlew bootRun
```

The first time I ran the command, I forgot to add the "sudo" command and this was the error message:

![build_fail](Part1/images/build_fail.png)


After this, I went to the **http://192.168.56.5:8080/** page again to check that everything was working.

Now to check the application which runs a chat server and chat clients, I navigated to directory
"**ca2/Par1/**".

Here, I repeated the steps of building the app using gradle:

```bash
$ chmod +x gradlew
$ sudo ./gradlew build
```

To run the chat server on the VM, I did not have to change anything on the tasks developed. Just ran the command:

```bash
$ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

To run the chat client on my host machine, I had to change the args from my **runClient** task inside the **build.gradle**
file on a cloned project, from:

```groovy
task runClient(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on localhost:59001 "
  
    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args 'localhost', '59001'
}
```

To:

```groovy
task runClient(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on localhost:59001 "
  
    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
}
```

By doing this, if I run the **runClient** task from a cloned project on my host machine, the task will make the 
chat client connect with the chat server running on the VM. The command used was:

```bash
$ ./gradlew runClient
```

The following image shows the successful connection of the chat client with the chat server.

![server_client_connection](Part1/images/server_client_connection.png)

The window on the left shows the chat server running on my VM. The window on the right shows the chat client
running on my host machine. The window at the bottom is the client chat window where we can chat.

The purpose of doing this is to test the real situation where a client connects with an external server (not
running on the same machine).

**This is the end of CA3 Part 1.**


## Part 2 - Virtualization with Vagrant

### 1. Analysis - VirtualBox as Hypervisor

Oracle VM VirtualBox is a cross-platform virtualization application. What does that mean? For
one thing, it installs on your existing Intel or AMD-based computers, whether they are running
Windows, Mac OS X, Linux, or Oracle Solaris operating systems (OSes). Secondly, it extends the
capabilities of your existing computer so that it can run multiple OSes, inside multiple virtual
machines, at the same time. As an example, you can run Windows and Linux on your Mac,
run Windows Server 2016 on your Linux server, run Linux on your Windows PC, and so on, all
alongside your existing applications. You can install and run as many virtual machines as you
like. The only practical limits are disk space and memory.
Oracle VM VirtualBox is deceptively simple yet also very powerful. It can run everywhere from
small embedded systems or desktop class machines all the way up to datacenter deployments
and even Cloud environments (*[VirtualBox User Manual][5]*).


VirtualBox is being actively developed with frequent releases and has an ever growing list of features, supported 
guest operating systems and platforms it runs on. VirtualBox is a community effort backed by a dedicated company: 
everyone is encouraged to contribute while Oracle ensures the product always meets professional quality criteria 
(*[virtualbox.org][3]*).

For a more in-depth analysis of this hypervisor, here is a 
[datasheet][4] and a [user manual][5].

### 2. Implementation with VirtualBox

For this implementation, I used my laptop running Linux Mint 20.1, which is based in Ubuntu 20.04.

I installed Vagrant and VirtualBox, while also making sure I had Git and JDK8 installed.

We can check the Vagrant version by doing:

```bash
$ vagrant -v
```

Before proceeding with the vagrant initialization, some changes needed to be made. I started by reworking the application
present in [ca2/Part2/demo](../ca2/Part2/demo) according to the changes made by the teacher to the app in
https://bitbucket.org/atb/tut-basic-gradle. In addition, I changed the root project name to "demo" as well as the
uses of "basic" by the teacher. In **settings.gradle**:

```groovy
rootProject.name = 'demo'
```

After the application is reworked. I could prepare the vagrant initialization. First, to create a default **Vagrantfile**:

```bash
$ vagrant init envimation/ubuntu-xenial
```

This will set the vm to use the "envimation/ubuntu-xenial" box present in the Vagrant Cloud.

After that, I copied the content of the Vagrantfile provided by the teacher in 
https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/Vagrantfile and added it to my Vagrantfile.

One additional change was needed. In the Vagrantfile, I had to change the remote repo that the "web" vm will clone 
and work on. To do this, I needed to set my remote repo to public first, and then add the following lines to the
Vagrantfile:

```
# Change the following command to clone your own repository!
git clone https://PedroSantos5@bitbucket.org/PedroSantos5/devops-20-21-1201776.git
cd devops-20-21-1201776/ca2/Part2/demo
```

I also needed to change the name of the .war file that should be generated, to comply with the root project name
discussed above:

```
# To deploy the war file to tomcat8 do the following command:
sudo cp ./build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
```

Vagrant is now ready to start up. We can achieve this by doing:

```bash
$ vagrant up
```

After everything is done, we can check the status of both vms created by doing:

```bash
$ vagrant status
```

Now, to actually see if both vms are doing what they are supposed to, we should access them through a browser in the 
host machine. To open the spring web application running in the "web" vm:

- http://localhost:8080/basic-0.0.1-SNAPSHOT/

or

- http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/

To open the H2 console running in the "db" vm:

- http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console

or 

- http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/h2-console

On my end, everything worked as expected. If you run `vagrant up` using the [Vagrantfile](Part2/VirtualBox/Vagrantfile) 
present in [ca2/Part2/VirtualBox](Part2/VirtualBox/) and have the required dependencies installed, you will achieve
the same outcome.

### 4. Analysis of an Alternative - Qemu/KVM as Hypervisor

QEMU is a generic and open source machine emulator and virtualizer. When used as a machine emulator, QEMU can run OSes and programs made for one machine (e.g. an ARM board) on 
a different machine (e.g. your own PC). By using dynamic translation, it achieves very good performance (*[Qemu Wiki][2]*).

When used as a virtualizer, QEMU achieves near native performance by executing the guest code directly on the host CPU. 
QEMU supports virtualization when executing under the Xen hypervisor or using the KVM kernel module in Linux. 
When using KVM, QEMU can virtualize x86, server and embedded PowerPC, 64-bit POWER, S390, 32-bit and 64-bit ARM, 
and MIPS guests (*[Qemu Wiki][2]*).

Kernel-based Virtual Machine (KVM) is an open source virtualization technology built into Linux®. 
Specifically, KVM lets you turn Linux into a hypervisor that allows a host machine to run multiple, 
isolated virtual environments called guests or virtual machines (VMs) (*[What is KVM][1]*).

KVM converts Linux into a type-1 (bare-metal) hypervisor. All hypervisors need some operating system-level 
components—such as a memory manager, process scheduler, input/output (I/O) stack, device drivers, security manager, 
a network stack, and more—to run VMs. KVM has all these components because it’s part of the Linux kernel. Every VM 
is implemented as a regular Linux process, scheduled by the standard Linux scheduler, with dedicated virtual hardware 
like a network card, graphics adapter, CPU(s), memory, and disks (*[What is KVM][1]*).

The main difference between this alternative to VirtualBox is that it can only be used on a host machine running Linux.

### 5. Implementation of an Alternative - Qemu/KVM as Hypervisor

For the implementation of this alternative, I again used my laptop running Linux Mint 20.1, 
which is based in Ubuntu 20.04.

I started by installing Qemu/KVM, and the vagrant plugin needed to allow vagrant to use Qemu/KVM as hypervisor.

First, I ran:

```bash
$ vagrant plugin install vagrant-kvm
```

I then realized that this plugin seems to be deprecated. To replace it, I installed the "libvirt" plugin instead:

```bash
$ sudo apt-get install libxslt-dev libxml2-dev libvirt-dev qemu-utils
$ vagrant plugin install vagrant-libvirt
```

This would install some libvirt dependencies and the actual libvirt plugin for vagrant. As some errors occured
I followed another [guide](https://ostechnix.com/how-to-use-vagrant-with-libvirt-kvm-provider/) and ran another 
command to install some more dependencies:

```bash
$ sudo apt install qemu libvirt-daemon-system libvirt-clients libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev ruby-libvirt ebtables dnsmasq-base
```

At this point, I cannot remember if I used some other command to instal KVM. Nevertheless, everything beyond this point
should work if a correct instalation of Qemu/KVM and the vagrant-libvirt plugin is achieved.

I also installed the "mutate" plugin which converts vagrant boxes to work with different providers:

```bash
$ vagrant plugin install vagrant-mutate
```

Now that the required installation steps were complete, all I had to do was make some changes on the Vagrantfile.
I started by changing the box to be used. Instead of "envimation/ubuntu-xenial" which does not support libvirt, I chose
the "generic/ubuntu1604" box, which runs the same version of ubuntu. Check the [Vagrantfile](Part2/KVM/Vagrantfile) to
see where those changes were applied. I also made a small change on lines:

```
# We set more ram memmory for this VM
web.vm.provider "virtualbox" do |v|
```

To:

```
# We set more ram memmory for this VM
webKVM.vm.provider "kvm" do |v|
```

As you can see, the other change was naming the VMs differently. As I ran the Vagrantfile on the same directory I used
for the virtualization using VirtualBox, VMs with the names "db" and "web" were already created, so I needed to name
them differently. These are all the changes I made to the Vagrantfile.

The next step was to run the `vagrant up` command while adding some other options:

```bash
$ sudo vagrant up --provider=libvirt
```

I had to ran with the "sudo" command because I was getting some "permission denied error". Setting the provider as
"libvirt" explicitly tells Vagrant to use the libvirt KVM to run the virtual machine.

Everything worked fine so I ran the "status" command just to confirm it:

```bash
$ sudo vagrant status
```

![vagrant_status](Part2/KVM/images/vagrant_status.png)

Finally, I checked that the application and the database were up by accessing the VMs through a browser on my
host machine, as I did with the implementation with VirtualBox:

http://localhost:8080/basic-0.0.1-SNAPSHOT/ to check the "webKVM" VM.

http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console to check the "dbKVM" VM.

Thankfully, everything was working as expected.

### 6. Personal Note

The implementation of the alternative was more straight-forward than what I was expecting. I only had to solve some
installation issues regarding dependencies, but after that everything worked fine as if I was using the previous hypervisor
(VirtualBox). I had no preference, and I only chose Qemu/KVM as alternative because I was working on a linux host and
decided to try something different. From what I heard, my colleagues had many challenges using Hyper-V as hypervisor 
alternative, as it does not accept the static ip configurations present in the Vagrantfile.


### 7. References (not rendered)

[1]: https://www.redhat.com/en/topics/virtualization/what-is-KVM

[2]: https://wiki.qemu.org/Main_Page

[3]: https://www.virtualbox.org/

[4]: https://www.oracle.com/a/ocom/docs/oracle-vm-virtualbox-ds-1655169.pdf

[5]: https://download.virtualbox.org/virtualbox/6.1.22/UserManual.pdf


