# Individual Repository for DevOps

This repository contains the files for all the class assignments of DevOps.

* [Class Assignment 1](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/ca1/)

* [Class Assignment 2](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/ca2/)

* [Class Assignment 3](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/ca3/)

* [Class Assignment 4](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/ca4/)

* [Class Assignment 5](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/ca5/)

* [Class Assignment 6](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/ca6/)
