package com.greglturnquist.payroll;

/**
 * Thrown to indicate that a method has been passed a null argument.
 */
public class NullArgumentException extends IllegalArgumentException{

    /**
     * Constructs a NullArgumentException with the specified detail message.
     * @param s - The detail message.
     */
    public NullArgumentException(String s){
        super(s);
    }
}
