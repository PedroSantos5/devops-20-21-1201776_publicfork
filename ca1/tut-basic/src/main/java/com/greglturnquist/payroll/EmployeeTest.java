package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;


class EmployeeTest {

    @org.junit.jupiter.api.Test
    void employee_validCase() {

        Employee result = new Employee("Pedro", "Santos", "a person", "student", "eu@gmail.com");


        assertNotNull(result);
    }

    @org.junit.jupiter.api.Test
    void setFirstName_invalidCase_nullArgument() {

        Assertions.assertThrows(NullArgumentException.class, () -> {
            Employee result = new Employee(null, "Santos", "a person", "student", "eu@gmail.com");
        });
    }

    @org.junit.jupiter.api.Test
    void setFirstName_invalidCase_emptyArgument() {

        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            Employee result = new Employee("", "Santos", "a person", "student", "eu@gmail.com");
        });
    }

    @org.junit.jupiter.api.Test
    void setLastName_invalidCase_nullArgument() {

        Assertions.assertThrows(NullArgumentException.class, () -> {
            Employee result = new Employee("Pedro", null, "a person", "student", "eu@gmail.com");
        });
    }

    @org.junit.jupiter.api.Test
    void setLastName_invalidCase_emptyArgument() {

        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            Employee result = new Employee("Pedro", "", "a person", "student", "eu@gmail.com");
        });
    }

    @org.junit.jupiter.api.Test
    void setDescription_invalidCase_nullArgument() {

        Assertions.assertThrows(NullArgumentException.class, () -> {
            Employee result = new Employee("Pedro", "Santos", null, "student", "eu@gmail.com");
        });
    }

    @org.junit.jupiter.api.Test
    void setDescription_invalidCase_emptyArgument() {

        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            Employee result = new Employee("Pedro", "Santos", "", "student", "eu@gmail.com");
        });
    }

    @org.junit.jupiter.api.Test
    void setJobTitle_invalidCase_nullArgument() {

        Assertions.assertThrows(NullArgumentException.class, () -> {
            Employee result = new Employee("Pedro", "Santos", "a person", null, "eu@gmail.com");
        });
    }

    @org.junit.jupiter.api.Test
    void setJobTitle_invalidCase_emptyArgument() {

        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            Employee result = new Employee("Pedro", "Santos", "a person", "", "eu@gmail.com");
        });
    }

    @org.junit.jupiter.api.Test
    void setEmail_invalidCase_nullArgument() {

        Assertions.assertThrows(NullArgumentException.class, () -> {
            Employee result = new Employee("Pedro", "Santos", "a person", "student", null);
        });
    }

    @org.junit.jupiter.api.Test
    void setEmail_invalidCase_emptyArgument() {

        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            Employee result = new Employee("Pedro", "Santos", "a person", "student", "");
        });
    }


    @org.junit.jupiter.api.Test
    void validateEmail_invalidCase_invalidPatternOne() {

        Assertions.assertThrows(InvalidEmailException.class, () -> {
            Employee result = new Employee("Pedro", "Santos", "a person", "student", "santos.gmail.com");
        });
    }

    @org.junit.jupiter.api.Test
    void validateEmail_invalidCase_invalidPatternTwo() {

        Assertions.assertThrows(InvalidEmailException.class, () -> {
            Employee result = new Employee("Pedro", "Santos", "a person", "student", "santos@@gmail.com");
        });
    }




}