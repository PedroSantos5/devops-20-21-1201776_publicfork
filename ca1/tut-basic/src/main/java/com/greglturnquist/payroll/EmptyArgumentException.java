package com.greglturnquist.payroll;

/**
 * Thrown to indicate that a method has been passed an empty argument (e.g. empty String).
 */
public class EmptyArgumentException extends IllegalArgumentException{

    /**
     * Constructs an EmptyArgumentException with the specified detail message.
     * @param s - the detail message.
     */
    public EmptyArgumentException(String s){
        super(s);
    }
}
