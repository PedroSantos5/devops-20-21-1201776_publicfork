/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobTitle;
	private String email;

	private Employee() {}

	/**
	 * Constructor of Employee.
	 * @param firstName String of the employee's first name.
	 * @param lastName String of the employee's last name.
	 * @param description String of the employee's description.
	 * @param jobTitle String of the employee's job title.
	 * @param email String of the employee's email address.
	 * @throws NullArgumentException to be thrown when an argument is null.
	 * @throws EmptyArgumentException to be thrown when an argument is empty.
	 * @throws InvalidEmailException to be thrown when an email string argument has an invalid pattern.
	 */
	public Employee(String firstName, String lastName, String description, String jobTitle, String email) throws NullArgumentException,
			EmptyArgumentException, InvalidEmailException {

		setFirstName(firstName);
		setLastName(lastName);
		setDescription(description);
		setJobTitle(jobTitle);
		setEmail(email);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(id, employee.id) &&
			Objects.equals(firstName, employee.firstName) &&
			Objects.equals(lastName, employee.lastName) &&
			Objects.equals(description, employee.description) &&
			Objects.equals(jobTitle, employee.jobTitle) &&
			Objects.equals(email, employee.email);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id, firstName, lastName, description, jobTitle, email);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the firstName after validation.
	 * @param firstName String of the employee's first name.
	 * @throws NullArgumentException to be thrown when an argument is null.
	 * @throws EmptyArgumentException to be thrown when an argument is empty.
	 */
	public void setFirstName(String firstName) throws NullArgumentException, EmptyArgumentException {

		validateString(firstName);

		this.firstName = firstName;
	}

	/**
	 * Validates a String passed as argument. It throws an custom exception if the String is null or empty.
	 * @param aString The string to be validated.
	 * @throws NullArgumentException to be thrown when an argument is null.
	 * @throws EmptyArgumentException to be thrown when an argument is empty.
	 */
	private void validateString(String aString) throws NullArgumentException, EmptyArgumentException{
		if (aString == null){
			throw new NullArgumentException("Passed string cannot be null.");
		}
		if (aString.isEmpty()){
			throw new EmptyArgumentException("Passed string cannot be empty.");
		}
	}

	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the lastName after validation.
	 * @param lastName String of the employee's last name.
	 * @throws NullArgumentException to be thrown when an argument is null.
	 * @throws EmptyArgumentException to be thrown when an argument is empty.
	 */
	public void setLastName(String lastName) throws NullArgumentException, EmptyArgumentException {

		validateString(lastName);

		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * Sets the employee's description.
	 * @param description The string description.
	 * @throws NullArgumentException to be thrown when an argument is null.
	 * @throws EmptyArgumentException to be thrown when an argument is empty.
	 */
	public void setDescription(String description) throws NullArgumentException, EmptyArgumentException {

		validateString(description);

		this.description = description;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	/**
	 * Sets the employee's jobTitle.
	 * @param jobTitle The string of the jobTitle.
	 * @throws NullArgumentException to be thrown when an argument is null.
	 * @throws EmptyArgumentException to be thrown when an argument is empty.
	 */
	public void setJobTitle(String jobTitle) throws NullArgumentException, EmptyArgumentException {

		validateString(jobTitle);

		this.jobTitle = jobTitle;
	}

	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email of the employee.
	 * @param email The email string.
	 * @throws NullArgumentException to be thrown when an argument is null.
	 * @throws EmptyArgumentException to be thrown when an argument is empty.
	 * @throws InvalidEmailException to be thrown when an email string argument has an invalid pattern.
	 */
	public void setEmail(String email) throws NullArgumentException, EmptyArgumentException, InvalidEmailException {

		validateEmail(email);

		this.email = email;
	}

	/**
	 * This method validates the email string pattern passed as argument.
	 * @param email The email string.
	 * @throws NullArgumentException to be thrown when an argument is null.
	 * @throws EmptyArgumentException to be thrown when an argument is empty.
	 * @throws InvalidEmailException to be thrown when an email string argument has an invalid pattern.
	 */
	private void validateEmail(String email) throws NullArgumentException, EmptyArgumentException,
			InvalidEmailException {

		validateString(email);

		// Extracted from https://www.geeksforgeeks.org/check-email-address-valid-not-java/
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
				"[a-zA-Z0-9_+&*-]+)*@" +
				"(?:[a-zA-Z0-9-]+\\.)+[a-z" +
				"A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex);

		if (!pat.matcher(email).matches()){
			throw new InvalidEmailException("Invalid email string pattern.");
		}
	}

	@Override
	public String toString() {
		return "Employee{" +
			"id=" + id +
			", firstName='" + firstName + '\'' +
			", lastName='" + lastName + '\'' +
			", description='" + description + '\'' +
			", jobTitle='" + jobTitle + '\'' +
			", email='" + email + '\'' +
			'}';
	}
}
// end::code[]
