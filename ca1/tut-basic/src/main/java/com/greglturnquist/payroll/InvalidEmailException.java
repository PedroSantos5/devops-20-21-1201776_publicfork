package com.greglturnquist.payroll;

/**
 * Thrown to indicate that a method has been passed an invalid email string argument.
 */
public class InvalidEmailException extends IllegalArgumentException{

    /**
     * Constructs an InvalidEmailException with the specified detail message.
     * @param s - The detail message.
     */
    public InvalidEmailException(String s){
        super(s);
    }

}
