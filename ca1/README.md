# Class Assignment 1 Report

The source code for this assignment is located in the folder [ca1/tut-basic](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/ca1/tut-basic) .

## 1. Analysis, Design and Implementation

### 1.1 Analysis - Git as Version Control

For this class assignment, we should use Git as a Version Control System (VCS).
Version control is a system that records changes to a file or set of files over time so that one can recall 
specific versions later. It allows one to revert selected files back to a previous state, revert the entire project 
back to a previous state, compare changes over time, see who last modified something that might be causing a problem, 
who introduced an issue and when, and more. Using a VCS also generally means that if one screws things up or loses files, 
it can all be easily recovered (*[Pro Git][1]*).

Git is a distributed version control system (opposed to a centralized version control system), which means that 
clients fully mirror a repository and its full history, instead of just checking out the latest snapshot of the files 
held in a single server (in centralized VCSs). Thus, if any server dies, and these systems were collaborating via that 
server, any of the client repositories can be copied back up to the server to restore it. Every clone is really a full
backup of all the data (*[Pro Git][1]*).

For every commit, Git saves the state of the project (e.g. all the files) as a snapshot which can be explained as
taking a picture of what all the files look like at that moment and stores a reference to that snapshot (*[Pro Git][1]*).

Git has three main states that files can reside in: modified, staged, and committed (*[Pro Git][1]*):

- Modified means that one has changed the file but has not committed it to the database yet;

- Staged means that one has marked a modified file in its current version to go into the next commit snapshot;

- Committed means that the data is safely stored in the local database.

There are three main sections of a Git project: the working tree, the staging area, and the Git directory.
The working tree is a single checkout of one version of the project. These files are pulled out of the compressed 
database in the Git directory and placed on disk for one to use or modify.
The staging area is a file, generally contained in one's Git directory, that stores information about what will go
into the next commit.
The Git directory is where Git stores the metadata and object database for one's project. This is the most 
important part of Git, and it is what is copied when one clones a repository from another computer (*[Pro Git][1]*).

Regarding Git branching, branches are a part of a development process. Git branches are effectively a pointer to 
a snapshot of one's changes. When someone wants to add a new feature or fix a bug, they should spawn a new 
branch to encapsulate the changes. This makes it harder for unstable code to get merged into the main code base, 
and it gives the chance to clean up future history before merging it into the main branch. In Git, the main branch
has the default name "**master**" (*[Using-branches][2]*).

For this particular assignment, we will use an already developed application named "Tutorial React.js and Spring Data Rest"
 that we will add to our Git repository and develop it further.

### 1.2 Design

As requested for this assignment, we should use our private remote repository on Bitbucket
(*[my repository](https://bitbucket.org/PedroSantos5/devops-20-21-1201776/src/master/)*).
We will work on the "basic" version of the application provided to us and mentioned in section [1.1 Analysis](#11-analysis---git-as-version-control).
Regarding our repository, we should have a "master" branch to "publish" the stable versions of our work on the 
application. This master branch will have tags to mark the different published versions, using the pattern 
major.minor.revision (e.g., 1.2.0). The inicial version will be marked with tag "**v1.2.0**".

New features will be developed under branches named after the feature. The first new feature to be developed will involve 
the creation of an email field on the Employee class, so the first new branch should be named "email-field".
An attribute email will be created on this Employee class, as well as methods for validation of all attributes also
present in said class. Unit tests will be developed to test these validation methods, and the creation of Employee objects.
After successfully testing the code developed, the code from this branch should be merged with the master branch, marking
the new version with tag "**v1.3.0**".

To debug the application in the IDE, we can use breakpoints in the code. If we click on the right of a number line, a red dot will
appear. We should choose the Debug option instead of Run when trying to run a method. The process will stop on the
line with the breakpoint. After that, we can manually run each line of code to understand step-by-step what is happening
during runtime for each line of code (e.g., variables being created).

![Breakpoint Example](../ca1/tut-basic/images/breakpoint_image.png)

To debug the application in the browser, we can use its developer tools (Shortcut: Ctrl+Shift+I).
By navigating to the Sources panel we can view:

![developer tools](https://javascript.info/article/debugging-chrome/chrome-tabs.svg "Developer tools window")

(extracted from https://javascript.info/debugging-chrome)

1. The **File Navigator** pane lists HTML, JavaScript, CSS and other files, including images that are attached to the page.
2. The **Code Editor** pane shows the source code.
3. The **JavaScript Debugging** pane is for debugging.

We should use that developer tools window after we actually start our application. This will be detailed in the
[1.3 Implementation using Git](#13-implementation-using-git) section.

A new branch should be created to fix a bug regarding the email field validation. This new branch should be called 
"fix-invalid-email" where we will add extra validation for the email string (e.g, an email must have the "@" sign.).
After developing the new code, which should also include testing, this created branch should be merged into the 
master branch, tagging the new version as "**v1.3.1**".

When the assignment is complete, the repository should be marked with tag "**ca1**".

### 1.3 Implementation using Git

#### *1.3.1 Part One - Adding the email field*

As a first step, Git should be downloaded from (https://git-scm.com/). We will be using **git bash** to input the required
commands to complete this assignment.

A template for the repository structure was provided here (https://bitbucket.org/atb/devops-21-rep-template/).

To use this structure as it is, a special clone command was used:

1. Open Git Bash.
2. Create a bare clone of the repository:

```bash
$ git clone --bare https://bitbucket.org/atb/devops-21-rep-template.git
```

3. Mirror-push to the new repository (in my case should be https://bitbucket.org/PedroSantos5/devops-20-21-1201776.git):
```bash
$ cd devops-21-rep-template
$ git push --mirror https://github.com/exampleuser/new-repository.git
```

4. Remove the temporary local repository created ealier:

```bash
$ cd ..
$ rm -rf devops-21-rep-template
```

After this, some work was done regarding the creation of a jobTitle field, developed in the second DevOps class. This will
not be addressed in this assignment as it is not part of it.

We should now tag the existing code in our repository as "**v1.2.0**":

```bash
$ git tag -a v1.2.0 -m "ca1 initial version"
```

This will tag the last commit made. We can confirm the tag existence using:

```bash
$ git tag
```

Or by showing the commit marked with that tag:

```bash
$ git show v1.2.0
```

Whether the last commit was already pushed or not, we must push the tag separately after pushing the commit: 

```bash
$ git push origin v1.2.0
```

At any given time, it is possible to check what is in our working tree, stating area, and Git directory by
issuing the command:

```bash
$ git status
```

To have a look at the commits made, use:

```bash
$ git log
```


We are now prepared to start working on the new features. To start, an issue should be created on Bitbucket where 
the first feature will be tracked and assigned to someone. In this case it will be me.

Lets then create a new branch where we will develop the first feature. The branch should be named "email-field":

```bash
$ git branch email-field
```

Now, we will switch the HEAD of our local repository to this new branch. HEAD is a pointer that identifies the 
active branch being worked on.

```bash
$ git checkout email-field
```

Alternatively, we could do the above commands in just one, where we create a branch and immediately switch to it, by
doing:

```bash
$ git checkout -b email-field
```

We can verify the existing branches by doing:

```bash
$ git branch
```

The active branch should appear marked with an asterisk.


We should now work on developing the code for this new feature. I started by creating custom exceptions to be thrown
in case an invalid argument is passed to the constructor of Employee. These are:

- NullArgumentException
- EmptyArgumentException

Here is the code for the NullArgumentException as an example:

```java
package com.greglturnquist.payroll;

/**
 * Thrown to indicate that a method has been passed a null argument.
 */
public class NullArgumentException extends IllegalArgumentException{

    /**
     * Constructs a NullArgumentException with the specified detail message.
     * @param s - The detail message.
     */
    public NullArgumentException(String s){
        super(s);
    }
}
```

Then, the setter methods where updated to validate the arguments before assigning them as Employee attributes, e.g:

```java
/**
 * Sets the firstName after validation.
 * @param firstName String of the employee's first name.
 * @throws NullArgumentException to be thrown when an argument is null.
 * @throws EmptyArgumentException to be thrown when an argument is empty.
 */
public void setFirstName(String firstName) throws NullArgumentException, EmptyArgumentException {

        validateString(firstName);

        this.firstName = firstName;
}
```

Being the validateString method:

```java
/**
 * Validates a String passed as argument. It throws an custom exception if the String is null or empty.
 * @param aString The string to be validated.
 * @throws NullArgumentException to be thrown when an argument is null.
 * @throws EmptyArgumentException to be thrown when an argument is empty.
 */
private void validateString(String aString) throws NullArgumentException, EmptyArgumentException{
        if (aString == null){
            throw new NullArgumentException("Passed string cannot be null.");
        }
        if (aString.isEmpty()){
            throw new EmptyArgumentException("Passed string cannot be empty.");
        }
}
```

After doing this for every attribute validation method, the arguments constructor should look like this:

```java
/**
 * Constructor of Employee.
 * @param firstName String of the employee's first name.
 * @param lastName String of the employee's last name.
 * @param description String of the employee's description.
 * @param jobTitle String of the employee's job title.
 * @param email String of the employee's email address.
 * @throws NullArgumentException to be thrown when an argument is null.
 * @throws EmptyArgumentException to be thrown when an argument is empty.
 */
public Employee(String firstName, String lastName, String description, String jobTitle, String email) 
        throws NullArgumentException, EmptyArgumentException {

        setFirstName(firstName);
        setLastName(lastName);
        setDescription(description);
        setJobTitle(jobTitle);
        setEmail(email);
}
```

We should create unit tests for these validation methods. Here is how we can test the valid case:

```java
class EmployeeTest {

 @org.junit.jupiter.api.Test
 void employee_validCase() {

  Employee result = new Employee("Pedro", "Santos", "a person", "student", "eu@gmail.com");


  assertNotNull(result);
 }
}
```

And as an example of an invalid situation:

```java
@org.junit.jupiter.api.Test
void setFirstName_invalidCase_nullArgument() {

    Assertions.assertThrows(NullArgumentException.class, () -> {
        Employee result = new Employee(null, "Santos", "a person", "student", "eu@gmail.com");
    });
}
```

To test the initialization of the application, and to be possible to debug it on a browser through the developer tools window
the app must first be booted. This can be done through **git bash**, by opening it inside the folder **ca1/tut-basic** where a file "mvnw" is located. We can then run the command:

```bash
$ ./mvnw spring-boot:run
```

Alternatively, we could start the application through the IDE by running the **main** method:

```java
package com.greglturnquist.payroll;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Greg Turnquist
 */
@SpringBootApplication
public class ReactAndSpringDataRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactAndSpringDataRestApplication.class, args);
	}
}
```


If we try to run the app by passing an invalid string to the default Employee object that is initially created, 
the exceptions will be thrown and the app will shutdown.

First with a null string passed as argument to the constructor:

```java
package com.greglturnquist.payroll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author Greg Turnquist
 */
@Component // <1>
public class DatabaseLoader implements CommandLineRunner { // <2>

	private final EmployeeRepository repository;

	@Autowired // <3>
	public DatabaseLoader(EmployeeRepository repository) {
		this.repository = repository;
	}

	@Override
	public void run(String... strings) throws Exception { // <4>
		this.repository.save(new Employee(null, "Baggins", "ring bearer", "Ring Destroyer",
				"frodobaggins@gmail.com"));
	}
}
```
The NullArgumentException thrown:

![NullArgumentException](../ca1/tut-basic/images/nullArgument_buildFailed.png)


And with an empty string passed as argument to the constructor:

```java
package com.greglturnquist.payroll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Component // <1>
public class DatabaseLoader implements CommandLineRunner { // <2>

	private final EmployeeRepository repository;

	@Autowired // <3>
	public DatabaseLoader(EmployeeRepository repository) {
		this.repository = repository;
	}

	@Override
	public void run(String... strings) throws Exception { // <4>
		this.repository.save(new Employee("", "Baggins", "ring bearer", "Ring Destroyer",
				"frodobaggins@gmail.com"));
	}
}
```

The EmptyArgumentException thrown:

![EmptyArgumentException](../ca1/tut-basic/images/emptyArgument_buildFailed.png)

After successfully booting the app, in order to see the result of the application running, we should type the 
following address in the browser address bar:

```
http://localhost:8080
```

The initial table should look like this:

![Initial table](../ca1/tut-basic/images/initial_table.png)

We can add a new Employee entry by issuing the following command with the desired fields:

```bash
$ curl -X POST localhost:8080/api/employees -d "{\"firstName\": \"Bilbo\", \"lastName\": \"Baggins\", \"description\": \"burglar\", \"jobTitle\": \"jobless\", \"email\": \"bilbo@gmail.com\"}" -H "Content-Type:application/json"
```

This is the output:

![Success output](../ca1/tut-basic/images/newEntry_success.png)

After refreshing the page, the new table should look like this:

![table after new entry](../ca1/tut-basic/images/table_after_newEntry.png)

If we try to add a new entry with an invalid first name (e.g **null**), the expected exception is thrown, 
the new Employee entry is not added to the table but the app **does not** shutdown:

```bash
$ curl -X POST localhost:8080/api/employees -d "{\"firstName\": null, \"lastName\": \"Baggins\", \"description\": \"burglar\", \"jobTitle\": \"jobless\", \"email\": \"bilbo@gmail.com\"}" -H "Content-Type:application/json"
```

The output warning:

![Fail output](../ca1/tut-basic/images/newEntry_failed.png)


If everything is working as expected, we should now add the files worked on to the staging area of Git, so they can
then be committed and pushed to our remote repository. We should start be doing:

```bash
$ git add .
```

By doing that, we added all the changes made to the code on our files, into the staging area. We can now commit
those changes to our local repository:

```bash
$ git commit -m "Added email-field, validation methods and unit tests."
```

The **-m** argument adds a customized comment to the commit made.

We can use the following command to check how the local repository differs from the remote repository. 

```bash
$ git branch -vv
```

The remote repository is not aware yet of our new branch **email-field**. We should set as the upstream branch on the 
remote repository the local branch created as **email-field**:

```bash
$ git push -u origin email-field
```

This will push the created branch **email-field** and the commit made, to our remote repository.

If we do not have any more changes to make on this branch, and the feature is complete, we can now merge the developed 
branch **email-field** into the **master** branch. To do so we must first switch the active branch back to the **master** 
branch by changing the HEAD pointer:

```bash
$ git checkout master
```

We must always change back to the **master** branch before doing the merge, so that the changes made in the 
**email-field** branch are incorporated into the **master** branch and not the other way around.

We can now proceed with the merge:

```bash
$ git merge email-field
```

Tag this new version of the application (tag as **v1.3.0**):

```bash
$ git tag -a v1.3.0 -m "Email-field added."
```

Now we should push this merged change:

```bash
$ git push
```

As well as the tag:

```bash
$ git push origin v1.3.0
```

As for the issue we created on Bitbucket, we could auto-resolve it by referencing it in the comment of a commit 
(see https://support.atlassian.com/bitbucket-cloud/docs/resolve-issues-automatically-when-users-push-code/), 
or by manually changing the state of the issue to **Resolved** while referencing the commit that "resolved" the issue.
I opted for the latter.

Optionally, we can delete the branch created for developing the new feature by using:

```bash
$ git branch -d email-field
```

I opted for not doing this yet as it may be useful to backtrack what was done.

*This concludes the first part of the assignment implementation using Git.*

#### *1.3.2 Part Two - fixing validation for the email field*

We should create a new issue in Bitbucket for developing a fix for the email-field validation.

Remember that we are now working on the **master** branch which now has the email field created in the Employee class
developed in [1.3.1 Part One](#131-part-one---adding-the-email-field). We must then create a new branch to develop the fix for the email field validation. 
The new branch should be named "**fix-invalid-email**":

```bash
$ git branch fix-invalid-email
```

Do not forget to switch the HEAD pointer to this new branch:

```bash
$ git checkout fix-invalid-email
```

Now we can work on the code to develop the fix for the email field validation. As described earlier in the [1.2 Design](#12-design)
section, we must ensure that an Employee is created with a valid email (must have the "@" sign).

To do this, I started by creating a new custom exception to be thrown when a passed email string is invalid:

```java
package com.greglturnquist.payroll;

/**
 * Thrown to indicate that a method has been passed an invalid email string argument.
 */
public class InvalidEmailException extends IllegalArgumentException{

    /**
     * Constructs an InvalidEmailException with the specified detail message.
     * @param s - The detail message.
     */
    public InvalidEmailException(String s){
        super(s);
    }

}
```

Then, developed a method for a specific email validation pattern:

```java
/**
 * This method validates the email string pattern passed as argument.
 * @param email The email string.
 * @throws NullArgumentException to be thrown when an argument is null.
 * @throws EmptyArgumentException to be thrown when an argument is empty.
 * @throws InvalidEmailException to be thrown when an email string argument has an invalid pattern.
 */
private void validateEmail(String email) throws NullArgumentException, EmptyArgumentException,
        InvalidEmailException {

        validateString(email);

        // Extracted from https://www.geeksforgeeks.org/check-email-address-valid-not-java/
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);

        if (!pat.matcher(email).matches()){
            throw new InvalidEmailException("Invalid email string pattern.");
        }
}
```

This method will first make use of the already existing method validateString to ensure that the string passed is not
null nor empty. Then, the string will be matched with a valid pattern for email addresses. The actual code pattern
was extracted from a website, and a reference to that website was made.

The email setter method can now call the created method to perform validation before setting the passed string as an
Employee email attribute:

```java
/**
 * Sets the email of the employee.
 * @param email The email string.
 * @throws NullArgumentException to be thrown when an argument is null.
 * @throws EmptyArgumentException to be thrown when an argument is empty.
 * @throws InvalidEmailException to be thrown when an email string argument has an invalid pattern.
 */
public void setEmail(String email) throws NullArgumentException, EmptyArgumentException, InvalidEmailException {

        validateEmail(email);

        this.email = email;
}
```

For testing, we can add tests for the newly created validation method in the EmployeeTest class:

```java
@org.junit.jupiter.api.Test
void validateEmail_invalidCase_invalidPatternOne() {
    
    Assertions.assertThrows(InvalidEmailException.class, () -> {
        Employee result = new Employee("Pedro", "Santos", "a person", "student", "santos.gmail.com");
    });
}

@org.junit.jupiter.api.Test
void validateEmail_invalidCase_invalidPatternTwo() {
    
    Assertions.assertThrows(InvalidEmailException.class, () -> {
        Employee result = new Employee("Pedro", "Santos", "a person", "student", "santos@@gmail.com");
    });
}
```

If we try to run the app by passing an invalid email string pattern to the default Employee object that
is initially created, the exception will be thrown, and the app will shutdown.

The invalid email string passed as argument to the constructor:

```java
package com.greglturnquist.payroll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author Greg Turnquist
 */
@Component // <1>
public class DatabaseLoader implements CommandLineRunner { // <2>

	private final EmployeeRepository repository;

	@Autowired // <3>
	public DatabaseLoader(EmployeeRepository repository) {
		this.repository = repository;
	}

	@Override
	public void run(String... strings) throws Exception { // <4>
		this.repository.save(new Employee("Frodo", "Baggins", "ring bearer", "Ring Destroyer",
				"frodo@baggins@gmail.com"));
	}
}
```

The InvalidEmailException thrown:

![InvalidEmailCrash](../ca1/tut-basic/images/invalidEmailPattern_buildFailed.png)


After making all the changes and ensuring everything is working as expected, we can stage those changes:

```bash
$ git add .
```

And commit the staged changes to the local repository:

```bash
$ git commit -m "Added validation for email pattern. Added unit tests for email pattern validation methods. Fixed typo in toString method. Fixed hashcode method."
```

Again, we should set the upstream branch on the remote repository as the branch we are working on now - **fix-invalid-email**:

```bash
$ git push -u origin fix-invalid-email
```

Before merging branches, we must switch back to the **master** branch:

```bash
$ git checkout master
```

After that, we are ready to merge the branches:

```bash
$ git merge fix-invalid-email
```

Place the tag "**v1.3.1**" for the new version of the application:

```bash
$ git tag -a v1.3.1 -m "Email pattern validation added to master branch."
```

Now, pushing the merge made:

```bash
$ git push
```
And the tag:

```bash
$ git push origin v1.3.1
```

We should close the issue created in Bitbucket as the fix was successfully implemented. Use one of the ways discussed earlier.

To see a graphic view of the branches created and developed, we can do:

```bash
$ git log --oneline --decorate --graph --all
```

We can add more arguments such as **--branches** and **--tags** to get more information displayed.

Again, we could optionally delete the created branch for this fix, by doing:

```bash
$ git branch -d fix-invalid-email
```

I opted for not doing this yet as it may be useful to backtrack what was done.

**The Git implementation assignment is now complete. After everything is done (markdown included), the repository should
be tagged "*ca1*".**


## 2. Analysis of an Alternative

### Mercurial as Version Control

The alternative to Git to be used as a second implementation of this class assignment will be **Mercurial**.

Mercurial is also a distributed version control system, the same as Git, but there are some key differences worth
pointing out, being the **branching structure** one of them.  

In Git, branches are only references to a certain commit. 
This makes them lightweight yet powerful. Git allows someone to create, delete, and change a branch anytime, 
without affecting the commits. If someone needs to test a new feature or find a bug — make a branch, do the changes, 
and then delete the branch (*[Git vs Mercurial][3]*). 

Branching in Mercurial doesn't share the same meaning. They refer to a linear line of consecutive changesets. 
Changesets (csets) refer to a complete set of changes made to a file in a repository. Mercurial embeds the branches 
in the commits, where they are stored forever. This means that branches cannot be removed because that would 
alter the history. However, one can refer to certain commits with bookmarks, and use them in a similar manner 
to Git’s branches (*[Git vs Mercurial][3]*).

As for another difference between the two VCS, Git supports the idea of a staging area, 
which is also known as the index file, while Mercurial does not (*[Git vs Mercurial][3]*).

Everything else is almost the same, including the commands. Although Mercurial also provides a GUI Workbench, I will be using
the Windows command line to mimic the behaviour of the Git commands on Git Bash. Thus, the Windows cmd should be running 
commands on the [ca1/tut-basic](/ca1/tut-basic) directory.


## 3. Implementation of the Alternative

### Implementation using Mercurial

*The implementation described here will be focused on the Mercurial version control, as the code changes for the 
assignment are the same as the ones described in the [Git Implementation](#13-implementation-using-git) section.*

#### *3.1 Part One - Adding the email field*

Since Bitbucket no longer supports Mercurial repositories, a remote repository alternative was chosen -  Helix Team Hub 
([my remote repo](https://helixteamhub.cloud/solitary-pot-4208/projects/Pedro_Santos_Mercurial/repositories/Mercurial_repo/tree/default)).

First, Mercurial should be downloaded from https://www.mercurial-scm.org/ .

After that we should set our username on a file called "**mercurial.ini**" by writing the following:

```
[ui]
username = Pedro Santos 1201776 <1201776@isep.ipp.pt>
```

Create a new local Mercurial repository with the name of the folder where the project will be.
Here I called it "project":

```shell
> hg init project
```

Now, I made beforehand a duplicate of the initial version of the files and code I had on the Git repository and copied
it into the "project" folder. (**Note:** I only copied files related to the assignment and excluded the .git folder of the
Git repository).

As my new Mercurial repository has now new files, we should run the following command to commit them:

```shell
> hg commit -m "initial mercurial commit"
```

We should add the tag "**v1.2.0**" to this first commit that reflects the initial version of the code. By default
every last commit has tag "**tip**" so the tag will be changed:

```shell
> hg tag v1.2.0
```

We can see the currently effective tags by doing:

```shell
> hg tags
```

To see the commits made, we can use:

```shell
> hg log
```

Now to push our local Mercurial repository to the remote one in HelixTeamHub:

```shell
> hg push https://1201776@helixteamhub.cloud/solitary-pot-4208/projects/Pedro_Santos_Mercurial/repositories/mercurial/Mercurial_repo
```

The above command will ask for the password to access the remote repository. Just input it normally.
The first commit is also pushed, along with the tag, which is denoted as another commit referencing the change of the
tag:

```
Added tag v1.2.0 for changeset d1da912fe9c3
```

At some point I noted that Mercurial does not automatically create an ignore file for its repository (Git has 
the ".gitignore" file), so I manually created one - ".hgignore".
I copied the content of the .gitignore file and pasted it inside the .hgignore file, but quickly realized that that is
not enough. At the start of the .hgignore file, we should add:

```
syntax: glob
```

We should save the file with ANSI enconding format, and without extension. The file will be automatically recognized as
a HGIGNORE file. All this is needed as the formatting of the file is different to that of the .gitignore file.

As this .hgignore file was created, I added the file and committed it:

```shell
> hg add
```
```shell
> hg commit -m "Added hgignore file."
```

I also tagged this commit before finally pushing it:

```shell
> hg tag v1.2.1
```
```shell
> hg push
```

Before starting to work on the code to implement the email field, an issue should be created in HelixTeamHub
much like we did in Bitbucket. After that a new branch should be created on our local repository:

```shell
> hg branch email-field
```
The default mercurial branch is called "**default**", and by creating a new branch, the active branch 
switches automatically to the new branch (no need to checkout like in Git).

We can check the active branch by doing:

```shell
> hg branch
```


I started by implementing only the custom exceptions and committed the changes after that, as it is necessary to
make a commit after creating a branch, so it can take effect:

```shell
> hg commit -m "First commit on email-field branch. Added custom exceptions."
```

I forgot to add the new exception classes, so I needed to rollback the commit:

```shell
> hg rollback
```

Then, repeated the process in the correct order:

```shell
> hg add
```
```shell
> hg commit -m "First commit on email-field branch. Added custom exceptions."
```

Now to push the newly created branch to the remote repository:

```shell
> hg push --new-branch
```

After that, I added the email field and validation methods, adding anything new, committing the changes and pushing them:

```shell
> hg add
```
```shell
> hg commit -m "Added email field and validation methods."
```
```shell
> hg push
```

After creating tests and testing the validation methods, I noticed some unwanted files created by running the tests and
building the application where being added to be committed when I issued the `hg add` command. 
This is something to avoid as we only want the repository to hold changes made to the code.
I had to use the following command to revert the addition of those files:

```shell
> hg revert --all
```
This had another side effect - it created copies of some files with the .orig sufix.
To avoid adding again all these unwanted files, I added the following lines to the .hgignore file:

```
ca1/tut-basic/node_modules/
*.orig
*.js*
```

After correcting the problem I pushed the next commit:

```shell
> hg add
```
```shell
> hg commit -m "Added unit tests."
```
```shell
> hg push
```

Before merging branches, we should update the repository's working directory to the specified changeset, which in this
case should be the **default** branch. This switches revisions.

```shell
> hg update default
```

Then proceed with the merge:

```shell
> hg merge email-field
```

After merging, a commit is needed to realize the merge:

```shell
> hg commit -m "email-field branch merge into default branch."
```

Now, to tag this last commit and push it to the remote repository:

```shell
> hg tag v1.3.0
```
```shell
> hg push
```
We should now close the issue created in HelixTeamHub by the same possible ways as we did in Bitbucket with Git.

*This concludes the first part of the assignment implementation alternative using Mercurial.*

#### *3.2 Part Two - fixing validation for the email field*

We should create a new issue in HelixTeamHub, regarding the next feature to be developed.

I started by creating a new branch called "**fix-invalid-email**":

```shell
> hg branch fix-invalid-email
```

As to realise the new branch, I just added the new InvalidEmailException class and proceeded with comitting the change:

```shell
> hg add
```

```shell
> hg commit -m "Added InvalidEmailException. Fixed hashcode method."
```

I also pushed the new branch **fix-invalid-email** to the remote repository:

```shell
> hg push --new-branch
```

After that, I made the final changes by adding email pattern validation methods and their unit tests:

```shell
> hg add
```
```shell
> hg commit -m "Added email pattern validation methods. Added unit tests for those methods."
```
```shell
> hg push
```

Now as for the last merge, we should repeat the steps from earlier:

```shell
> hg update default
```
```shell
> hg merge fix-invalid-email
```
Commit the merge:
```shell
> hg commit -m "fix-invalid-email branch merged into default branch."
```
Tag the commit of the merge with "**v1.3.1**":
```shell
> hg tag v1.3.1
```
Finally, push the commit to the remote repository:
```shell
> hg push
```

To have a view of the repository tree, we can use the following command:

```shell
> hg log --graph
```

On HelixTeamHub, we should close the final issue while referencing the commit that resolved it.

Regarding the branches created on the Mercurial repository, contrary to Git, we **cannot** remove them because that
would alter the history.

**The alternative Mercurial implementation assignment is now complete. After everything is done (markdown included), 
the repository should be tagged "ca1".**

## 4. Personal Note

I liked the idea of Git commit snapshots and the existence of a staging area, but also the permanent 
nature of the branches created with Mercurial.

I had more problems working with Mercurial, specially regarding the .hgignore file that caused the repository
to mess with many unwanted files. I did not have the chance to use the Mercurial GUI workbench nor the Git GUI, 
which probably facilitates the use of these version control tools. Nevertheless, I am familiarized with IDE 
integrations of version control systems, which work for both Git and Mercurial.


## 5. References (not rendered)

[1]: https://git-scm.com/book/en/v2

[2]: https://www.atlassian.com/git/tutorials/using-branches

[3]: https://www.perforce.com/blog/vcs/git-vs-mercurial-how-are-they-different